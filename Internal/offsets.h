#pragma once

namespace offsets
{
	enum class clock_facade
	{
		INSTANCE = 0x1C5D7FC,
		GET_GAME_TIME = 0x34e6fd4,
		GAME_TIME = 0x34e6fd4,
		GET_EXE_TIME = 0x8C2EE0,
		FRAME_CLOCK = 0x8,
	};

	enum class device_handler
	{
		INSTANCE = 0x22fee94,
		DEVICE_PTR = 0x18,
		DIRECT_3D_DEVICE = 0x208
	};

	enum class manager_template
	{
		UNKNOWN_LIST = 0x16B2AB8,
		AI_BASES = 0x289E8CC,
		// "towercautionRing_Minimal_Fountain_White"	---> 2 above;		 mov     dword_3325A48, esi
		TURRETS = 0x30ec558,
		INHIBITOR = 0x2F4F888,
		// 89 44 24 18 A1 ? ? ? ? 53 55				---> first address;       mov     eax, dword_
		HEROES = 0x1859fac,
		// 8B 0D ?? ?? ?? ?? E8 ?? ?? ?? ?? EB 09		--->				 mov     ecx, dword_
		MINIONS = 0x185c0b4,
		// C7 83 ? ? ? ? ? ? ? ? 8B 15 ? ? ? ? 85 D2		---> first address;       mov     edx, dword
		MISSILES = 0x185c0ac,
		IS_ALIVE = 0x13b360,
		IS_INHIBITOR = 0x1951F0,
		IS_NEXUS = 0x1952F0,
		IS_MINION = 0x1953B0,
		IS_TURRET = 0x1955E0,
		IS_TARGETABLE = 0x1BD200,
		IS_DRAGON = 0x15A850,
		IS_BARON = 0x15C520,
		IS_HERO = 0x195370,
		IS_MISSILE = 0x1953D0,
		IS_TROY = 0xA40600
	};

	enum class game_client
	{
		// A1 ? ? ? ? 68 ? ? ? ? 8B 70 08 // dword_[offset]
		INSTANCE = 0x22FEE94,
		// never changes
		GAME_STATE = 0x8,
		//8B 0D ? ? ? ? 6A 01 FF 74 24 08 // dword_[offset] // 8B ?? ?? ?? ?? ?? 6A ?? 50 E8 ?? ?? ?? ?? 33 C0 5F C2 // dword_[offset]
		CHAT_CLIENT_PTR = 0x22FF400,
		//E8 ? ? ? ? 6A 00 68 ? ? ? ? E8 ? ? ? ? 83 C4 04 // sub_[offset]
		PRINT_CHAT = 0x546F30,
		//A1 ? ? ? ? 56 6A FF // sub_[offset]
		SEND_CHAT = 0x58A6D0
	};

	enum class object_manager
	{
		INSTANCE = 0x185c01c,// CHECK + UPDATED
		//E8 ? ? ? ? 8B F8 85 FF 0F 84 ? ? ? ? 53 8B 5C // sub_[offset]
		GET_FIRST_OBJECT = 0x269390,// CHECK + UPDATED
		//E8 ? ? ? ? 8B F0 85 F6 75 E4 // sub_[offset]
		GET_NEXT_OBJECT = 0x26B110, // CHECK + UPDATED
		GET_OBJECT_BY_INDEX = 0x2BA1A0,
		GET_OBJECT_BY_NETWORK_ID = 0x2BC7F0,
		LOCAL_PLAYER = 0x30F88DC,
		OBJECTS_ARRAY = 0x14,
		MAX_OBJECTS = 0x4 + 0x8,
	};

	enum class render_layer
	{
		INSTANCE = 0x02f73e78,
		// 83 EC 10 56 E8 ? ? ? ? 8B 08 // fn 
		WORLD_TO_SCREEN = 0x91BA50,
		CLIENT_WIDTH = 0x10,
		CLIENT_HEIGHT = 0x14,
		VIEW_MATRIX = 0x60,
		PROJECTION_MATRIX = 0xA0
	};

	enum class riot_window
	{
		INSTANCE = 0x2f4f3cc,
	};

	///////////////////////////////////FUNCTIONS//////////////////////////////////////

	enum class functions
	{
		CHANGER_CHARACTER_DATA = 0x3506498,
		ISSUE_ORDER = 0x15C5D0,
		ON_PROCESS_SPELL_W = 0x4D7940,
		ON_FINISH_CAST = 0x00504830,
		ON_STOP_CAST = 0x4F0130,
		NEW_CAST_SPELL = 0x4D5930,
		ON_CREATE_OBJECT = 0x2794E0,
		ON_DELETE_OBJECT = 0x26A4F0,
		ON_NEW_PATH = 0x2882d0,
		GET_ATTACK_CAST_DELAY = 0x2846C0,
		GET_ATTACK_DELAY = 0x2847C0,
		GET_BASIC_ATTACK = 0x14FD80,
		IS_NOT_WALL = 0x899380,
		IS_WALL = 0x8e4710,
		RET_ADDRESS = 0x6E502
	};

	// IsWall can use !IsNotwall func instead
	enum class nav_grid
	{
		INSTANCE = 0x035103d8,
		GET_CELL = 0x008d9ac0,
		GET_HEIGHT_FOR_POSITION = 0x8E2010
	};

	enum class ai_manager_client
	{
		CREATE_PATH = 0x1C4B60,
		CALCULATE_PATH = 0x008d6c40,
		SMOOTH_PATH = 0x1BD750,
	};

	///////////////////////////////////STRUCTS///////////////////////////////////////

	enum class hud_cursor_target_logic
	{
		CURSOR_POSITION = 0x1C,
		HOVERED_UNIT_ID = 0x3C,
	};

	enum class game_object
	{
		VIRTUAL_GET_BOUNDING_RADIUS = 36,
		VIRTUAL_GET_PATH_CONTROLLER = 148,

		CLASS_DATA = 0x4,
		ID = 0x20,
		TEAM = 0x4C,
		FLAGS = 0x40,
		NAME = 0x6C,
		NETWORK_ID = 0xCC,
		IS_DEAD = 0x1D4,
		POSITION = 0x1D8,
		IS_VISIBLE = 0x270,
		SOURCE_ID = 0x290,
		RESOURCE = 0x298,
		MAX_RESOURCE = RESOURCE + 0x10,
		RESOURCE_TYPE = MAX_RESOURCE + 0x40,
		SECONDARY_RESOURCE = RESOURCE_TYPE + 0x20,
		MAX_SECONDARY_RESOURCE = SECONDARY_RESOURCE + 0x10,
		LIFE_TIME = 0xC6C,
		STATUS_FLAGS = 0x3D0,
		IS_TARGETABLE = 0xD00,
		IS_UNTARGETABLE_TO_ALLIES = IS_TARGETABLE + 0x10,
		IS_UNTARGETABLE_TO_ENEMIES = IS_UNTARGETABLE_TO_ALLIES + 0x10,
		HEALTH = 0xD98,
		MAX_HEALTH = HEALTH + 0x10,
		ALL_SHIELD = 0xDD8,
		PHYSICAL_SHIELD = ALL_SHIELD + 0x10,
		MAGICAL_SHIELD = PHYSICAL_SHIELD + 0x10,
		ACTION_STATE = 0x1054,
		CHARACTER_INTERMEDIATE = 0x1670,
		COMBAT_TYPE = 0x2058,
		BUFFS = 0x2160 + 0x10,
		SPELL_BOOK = 0x2708,
		CHARACTER_DATA_STACK = 0x2F60,
		BASE_CHARACTER_DATA = 0x2EDC,
		CHARACTER_DATA = BASE_CHARACTER_DATA + 0x4,
		SKIN_NAME = 0x3134,
		ATTACK_DATA = 0x2ED0,
		EXPERIENCE = 0x36BC - 0x10,
		LEVEL = EXPERIENCE + 10,
		HERO_INVENTORY = 0x36F0,
	};

	enum class missile_client
	{
		MISSILE_SPELL_CAST_INFO = 0x230,
		SOURCE_ID = 0x28C,
		NETWORK_ID = 0x29C,
		TARGET_ID = 0x2E8,
		START_POS = 0x2A8,
		END_POS = 0x2B4,
		CAST_DELAY = 0x6E8,
		DELAY = 0x6F8,
		IS_BASIC_ATTACK = 0x708,
		IS_SPECIAL_ATTACK = IS_BASIC_ATTACK + 0x4,
		SLOT = 0x710,
		START_TIME = 0x75C,
	};

	enum class particle_client
	{
		LIFE_TIME = 0x264,
		EXPIRE_TIME = 0x5D0,
	};

	enum class class_data
	{
		TYPE = 0x0,
		NAME = 0x68
	};

	enum class spell_cast_info
	{
		SPELL_DATA = 0x0,
		SPELL_INFO = 0x8,
		LEVEL = 0x54,
		START_POSITION = 0x80,
		END_POSITION = 0x8C,
		SOUR�E_NETWORK_ID = 0x74,
		HAS_TARGET = 0xBC,
		SOURCE_ID = 0x6C,
		TARGET_ID = 0xC0,
		CAST_DELAY = 0x4C0,
		DELAY = 0x4D0,
		COOLDOWN = 0x4D4,
		IS_WINDING_UP = 0x4DC,
		IS_BASIC_ATTACK = 0x4E0,
		IS_SPECIAL_ATTACK = IS_BASIC_ATTACK + 0x4,
		SLOT = 0x4E8,
		END_TIME = 0x52C,
		CAST_END_TIME = 0x530,
		START_TIME = 0x544,
		SPELL_WAS_CAST = 0x538,
		IS_STOPPED = 0x53C,
		IS_INSTANT_CAST = 0x540,
	};

	enum class spell_info
	{
		LEVEL = 0x54,
		SOUR�E_NETWORK_ID = 0x6C,
		START_POSITION = 0x78,
		END_POSITION = 0x84,
		HAS_TARGET = 0xB4,
		SOURCE_ID = 0x64,
		TARGET_ID = 0xB8,
		CAST_DELAY = 0x4B8,
		DELAY = 0x4C8,
		COOLDOWN = 0x4D4,
		IS_WINDING_UP = 0x4D4,
		IS_BASIC_ATTACK = 0x4D8,
		IS_SPECIAL_ATTACK = IS_BASIC_ATTACK + 0x4,
		SLOT = 0x4E0,
		END_TIME = 0x524,
		CAST_END_TIME = 0x528,
		START_TIME = 0x53C,
		SPELL_WAS_CAST = 0x538,
		IS_STOPPED = 0x534,
		IS_INSTANT_CAST = 0x538,
	};

	enum class spell_data
	{
		SCRIPT = 0xC,
		NAME = 0x18,
		RESOURCE = 0x44,
	};

	enum class spell_data_script
	{
		NAME = 0x8,
		HASH = 0x88
	};

	enum class spell_data_resource
	{
		MISSILE_NAME = 0x64,
		SPELL_NAME = 0x88,
		DISPLAY_NAME_LOCALIZATION_KEY = 0x1F0,
		EFFECT_1 = 0xA0,
		EFFECT_2 = EFFECT_1 + 0x1C,
		EFFECT_3 = EFFECT_2 + 0x1C,
		EFFECT_4 = EFFECT_3 + 0x1C,
		EFFECT_5 = EFFECT_4 + 0x1C,
		EFFECT_6 = EFFECT_5 + 0x1C,
		EFFECT_7 = EFFECT_6 + 0x1C,
		EFFECT_8 = EFFECT_7 + 0x1C,
		EFFECT_9 = EFFECT_8 + 0x1C,
		EFFECT_10 = EFFECT_9 + 0x1C,
		EFFECT_11 = EFFECT_10 + 0x1C,
		A_EFFECT_1 = 0x200,
		A_EFFECT_2 = 0x204,
		CAST_TIME = 0x258,
		CANT_CANCEL_WHILE_WINDING_UP = 0x35C,
		CANT_CANCEL_WHILE_CHANNELING = 0x36C,
		CHANNEL_IS_INTERRUPTED_BY_ATTACKING = 0x370,
		CAN_MOVE_WHILE_CHANNELING = 0x378,
		MISSILE_SPEED = 0x440,
		SPELL_DATA_SPELL_WIDTH = 0x474,
		SPELL_DATA_SPELL_RADIUS = 0x3EC,
		CAST_RANGE = 0x3B4,
		CAST_RANGE_DISPLAY_OVERRIDE = 0x3B8,
		SPELL_DATA_EFFECT_SPELL_RANGE = 0x10C,
		SPELL_DATA_EFFECT_SPELL_RANGE_AFTER_EFFECT = 0xB8
	};

	enum class character_intermediate
	{
		PASSIVE_COOLDOWN_END_TIME = 0x30,
		PASSIVE_COOLDOWN_TOTAL_TIME = 0x40,
		PERCENT_DAMAGE_TO_BARRACKS_MINION_MOD = 0x16A0,
		FLAT_DAMAGE_REDUCTION_FROM_BARRACKS_MINION_MOD = 0x16B0,
		FLAT_ATTACK_DAMAGE_MOD = 0x80,
		PERCENT_ATTACK_DAMAGE_MOD = 0x90,
		PERCENT_BONUS_ATTACK_DAMAGE_MOD = 0xA0,
		FLAT_ABILITY_POWER_MOD = 0xC0,
		PERCENT_ABILITY_POWER_MOD = 0xD0,
		FLAT_MAGIC_REDUCTION = 0xE0,
		PERCENT_MAGIC_REDUCTION = 0xF0,
		ATTACK_SPEED_MOD = 0x110,
		BASE_ATTACK_DAMAGE = 0x140,
		FLAT_BASE_ATTACK_DAMAGE_MOD = 0x160,
		PERCENT_BASE_ATTACK_DAMAGE_MOD = 0x170,
		BASE_ABILITY_POWER = 0x180,
		ARMOR = 0x1D0,
		BONUS_ARMOR = 0x1E0,
		MAGIC_RESIST = 0x1F0,
		BONUS_MAGIC_RESIST = 0x200,
		MOVE_SPEED = 0x230,
		ATTACK_RANGE = 0x250,
		PHYSICAL_LETHALITY = 0x290,
		PERCENT_ARMOR_PENETRATION = 0x2A0,
		PERCENT_BONUS_ARMOR_PENETRATION = 0x2B0,
		FLAT_MAGIC_PENETRATION = 0x2E0,
		PERCENT_MAGIC_PENETRATION = 0x300,
		PERCENT_BONUS_MAGIC_PENETRATION = 0x310,
	};

	enum class buff_instance
	{
		TYPE = 0x4,
		SCRIPT = 0x8,
		START_TIME = 0xC,
		EXPIRE_TIME = 0x10,
		SCRIPT_INFO = 0x20,
		IS_PERMANENT = 0x70,
		COUNTER = 0x74,
		F_COUNTER = 0x2C
	};

	enum class script
	{
		VIRTUAL_GET_DISPLAY_NAME = 14,

		NAME = 0x8,
		TYPE = 0x4,
		BASE_STACKS_ALT = 0x20,
		STACKS_ALT = 0x24,
		HASH = 0x88
	};

	enum class buff_script_instance
	{
		CASTER_ID = 0x4
	};

	enum class spell_book_client
	{
		// E8 ? ? ? ? 8B F8 8B CB 89 // sub_[offset]
		GET_SPELL_STATE = 0x4C5E70,

		Virtual_GetSpellState = 1,
		Virtual_GetSpell = 19,
		ACTIVE_SPELL_INSTANCE = 0x20
	};

	enum class spell_instance_client
	{
		CAST_INFO = 0x8
	};

	enum class spell_data_inst
	{
		LEVEL = 0x20,
		IS_LEARNED = 0x24,
		CAST_TIME = 0x28,
		AMMO = 0x31C,
		AMMO_RECHARGE_TIME = 0x354,
		AMMO_CD = 0x68,
		TOGGLE_STATE = 0x70,
		SPELL_CD = 0x78,
		EFFECT_1 = 0x88,
		EFFECT_2 = EFFECT_1 + 0x4,
		EFFECT_3 = EFFECT_2 + 0x4,
		EFFECT_4 = EFFECT_3 + 0x4,
		EFFECT_5 = EFFECT_4 + 0x4,
		EFFECT_6 = EFFECT_5 + 0x4,
		EFFECT_7 = EFFECT_6 + 0x4,
		EFFECT_8 = EFFECT_7 + 0x4,
		EFFECT_9 = EFFECT_8 + 0x4,
		EFFECT_10 = EFFECT_9 + 0x4,
		EFFECT_11 = EFFECT_10 + 0x4,
		SPELL_DATA = 0x134,
		//"SpellDataInstClient::SetSpellData: %s n"...
	};

	enum class character_data_stack
	{
		SKIN_NAME = 0xC,
		SKIN_ID = 0x0EBC // E8 ? ? ? ? 83 C4 0C 80 BF // scroll down, instruction: cmp byte ptr [xxx+[offset]h], 0
	};

	enum class character_data
	{
		SKIN_NAME = 0x4,
		SKIN_HASH = 0x10,
		PROPERTIES = 0x1C
	};

	enum class character_properties
	{
		ATTACK_RANGE = 0x1CC,
	};

	enum class experience
	{
		LEVEL = 0x10,
	};

	enum class hero_inventory_common
	{
		SLOTS = 0x18,
		SLOTS6 = 0x18,
		SLOTS7 = 0x1C,
		SLOTS8 = 0x20,
		SLOTS9 = 0x24,
		SLOTS10 = 0x28,
		SLOTS11 = 0x2C,
		SLOTS12 = 0x30,
	};

	enum class inventory_slot
	{
		STACKS = 0x0,
		ITEM_INFO = 0xC
	};

	enum class item_info
	{
		ITEM_DATA = 0x20,
		AMMO = 0x24,
	};

	enum class item_data
	{
		ITEM_ID = 0x68,
		MAX_STACKS = 0xA8,
		PRICE = 0x4A0,
		DISPLAY_NAME_LOCALIZATION_KEY = 0x4A4
	};

	enum class path_controller_common
	{
		NAV_MESH = 0x100,
		HAS_NAVIGATION_PATH = 0x198,
		NAVIGATION_PATH = 0x19C,
		SERVER_POSITION = 0x2BC,
	};

	enum class navigation_path
	{
		INDEX = 0x0,
		START_POSITION = 0x8,
		END_POSITION = START_POSITION + 0xC,
		PATH = END_POSITION + 0xC,
		DASH_SPEED = 0x1D0,
		IS_DASHING = 0x1EC
	};

	enum class avatar_client
	{
		HAS_RUNES = 0x1E4,
		RUNES = 0x300,
	};

	enum class rune_instance
	{
		ID = 0x4,
	};
}
