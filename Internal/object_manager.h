#pragma once

template<typename T>
struct entity_list_t
{
	char pad_0000[0x4]; //0x0000
	T** entities;
	size_t size;
	size_t max_size;
};

class c_object_manager
{
public:
	static c_object* get_first_object()
	{
		using fnGetFirst = c_object* (__thiscall* )(void*);
		return ((fnGetFirst)(BASE_ADDRESS + static_cast<DWORD>(offsets::object_manager::GET_FIRST_OBJECT)))(*reinterpret_cast<void**>((BASE_ADDRESS + static_cast<DWORD>(offsets::object_manager::GET_FIRST_OBJECT))));
	}

	static c_object* get_next_object(c_object* object)
	{
		using fnGetNext = c_object* (__thiscall* )(void*, c_object*);
		return ((fnGetNext)(BASE_ADDRESS + static_cast<DWORD>(offsets::object_manager::GET_NEXT_OBJECT)))(*reinterpret_cast<void**>((BASE_ADDRESS + static_cast<DWORD>(offsets::object_manager::GET_NEXT_OBJECT))), object);
	}

	static entity_list_t<c_object>* get_heroes()
	{
		return *(entity_list_t<c_object>**)(BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::HEROES));
	}

	static entity_list_t<c_object>* get_minions()
	{
		return *(entity_list_t<c_object>**)(BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::MINIONS));
	}

	static entity_list_t<c_object>* get_missiles()
	{
		return *(entity_list_t<c_object>**)(BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::MISSILES));
	}

	static c_object* get_local()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::object_manager::LOCAL_PLAYER);

		return *(c_object**)address;
	}
};

inline c_object_manager object_manager;
