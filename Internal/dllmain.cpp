#include "pch.h"

void test()
{
    /*const auto heroes = object_manager.get_heroes();
    const auto minions = object_manager.get_minions();

    printf("heroes size - %d\tminions size - %d\n", heroes->size, minions->size);*/
}

int __stdcall main(const int base)
{
    MessageBoxA(nullptr, "Injected", "", 0);

    AllocConsole();
    freopen_s(reinterpret_cast<FILE**>(stdin), "CONIN$", "r", stdin);
    freopen_s(reinterpret_cast<FILE**>(stdout), "CONOUT$", "w", stdout);

    while (true)
    {
        if (GetAsyncKeyState(VK_DELETE) & 1)
        {
            break;
        }

        if (GetKeyState(VK_DELETE) & 1)
        {
            break;
        }

        test();

        using namespace std::chrono_literals;
        std::this_thread::sleep_for(0.25s);
    }

    fclose(stdin);
    fclose(stdout);
    FreeConsole();

    FreeLibraryAndExitThread(static_cast<HMODULE>(reinterpret_cast<PVOID>(base)), 1);
}

BOOL APIENTRY DllMain(HMODULE module, const DWORD ul_reason_for_call, LPVOID reserved)
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
    {
        CreateThread(nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(main), module, 0, nullptr);
    }

    if (ul_reason_for_call == DLL_PROCESS_DETACH)
    {
        MessageBoxA(nullptr, "Unloaded", "", 0);
    }

	return TRUE;
}
