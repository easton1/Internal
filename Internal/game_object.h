#pragma once

class c_object
{
public:
	bool is_alive()
	{
		using fnIsAlive = bool(__thiscall* )(c_object* obj);

		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_ALIVE);

		return ((fnIsType)address)(this);
	}

	using fnIsType = bool(__cdecl*)(c_object* obj);

	bool is_turret()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_TURRET);

		return ((fnIsType)address)(this);
	}

	bool is_hero()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_HERO);

		return ((fnIsType)address)(this);
	}

	bool is_missile()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_MISSILE);

		return ((fnIsType)address)(this);
	}

	bool is_nexus()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_NEXUS);

		return ((fnIsType)address)(this);
	}

	bool is_inhibitor()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_INHIBITOR);

		return ((fnIsType)address)(this);
	}

	bool is_baron()
	{
		static const auto address = BASE_ADDRESS + static_cast<DWORD>(offsets::manager_template::IS_BARON);

		return ((fnIsType)address)(this);
	}
};
