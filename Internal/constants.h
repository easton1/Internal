#pragma once

#define STR_MERGE_IMPL(x, y)				x##y
#define STR_MERGE(x,y)						STR_MERGE_IMPL(x,y)
#define MAKE_PAD(size)						BYTE STR_MERGE(pad_, __COUNTER__) [ size ]
#define DEFINE_MEMBER_0(x)					x;
#define DEFINE_MEMBER_N(x,offset)			struct { MAKE_PAD((DWORD)offset); x; };

enum class e_game_object_team
{
	NONE,
	ORDER = 100,
	CHAOS = 200,
	NEUTRAL = 300
};

enum class e_game_object_flags
{
	AI_BASE_CLIENT = 1 << 10,
	AI_MINION_CLIENT = 1 << 11,
	AI_HERO_CLIENT = 1 << 12,
	AI_TURRET_CLIENT = 1 << 13,
	MISSILE_CLIENT = 1 << 15,
};

enum class e_game_object_resource_type : BYTE
{
	MANA,
	ENERGY,
	NONE,
	SHIELD,
	BATTLE_FURY,
	DRAGON_FURY,
	RAGE,
	HEAT,
	GNAR_FURY,
	FEROCITY,
	BLOOD_WELL,
	WIND,
	OTHER,
};

enum class e_game_object_status_flags
{
	INVULNERABLE = 1 << 0,
	MAGIC_IMMUNE = 1 << 6,
	PHYSICAL_IMMUNE = 1 << 7,
};

enum class e_game_object_action_state
{
	CAN_ATTACK = 1 << 0,
	CAN_CRIT = 1 << 1,
	CAN_CAST = 1 << 2,
	CAN_MOVE = 1 << 3,
	IMMOVABLE = 1 << 4,
	STEALTHED = 1 << 5,
	OBSCURED = 1 << 6,
	TAUNTED = 1 << 7,
	FEARED = 1 << 8,
	FLEEING = 1 << 9,
	SUPPRESSED = 1 << 10,
	SLEEP = 1 << 11,
	GHOSTED = 1 << 13,
	CHARMED = 1 << 17,
	SLOWED = 1 << 24,
};

enum class e_dampener_state
{
	ALIVE,
	DESTROYED
};

enum class e_game_object_combat_type
{
	MELEE = 1,
	RANGED,
};

enum class e_game_object_order
{
	NONE = 0,
	HOLD_POSITION,
	MOVE_TO,
	ATTACK_UNIT,
	AUTO_ATTACK_PET,
	AUTO_ATTACK,
	MOVE_PET,
	ATTACK_TO,
	STOP = 10,
};

