#ifndef PCH_H
#define PCH_H

#define WIN32_LEAN_AND_MEAN

#define BASE_ADDRESS (DWORD)GetModuleHandle(NULL)

#include <Windows.h>

#include <thread>
#include <chrono>
#include <memory>
#include <unordered_map>
#include <emmintrin.h>
#include <immintrin.h>
#include <cstddef>

#include "offsets.h"
#include "constants.h"

#include "spell_book.h"

#include "game_object.h"
#include "object_manager.h"

#endif //PCH_H
